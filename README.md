## Для запуска локально нужно выполнить следующие шаги:
- Склонировать проект  
`git clone https://gitlab.com/garm8/idaproject.git`  
  или  
`git clone git@gitlab.com:garm8/idaproject.git`  
- Собрать проект в docker-образ  
`docker build --no-cache --force-rm --tag idaproject:latest .`
- Запустить контейнер с образом проекта  
`docker run -p 8000:8000 idaproject:latest`  
- Открыть стартовую страницу  
`0.0.0.0:8000/images/`
