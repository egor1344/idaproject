FROM python:3-slim
WORKDIR /usr/src/idaproject/
COPY . .
RUN pip install -r requirements.txt
RUN python manage.py migrate --no-input
EXPOSE 8000
CMD ["gunicorn", "idaproject.wsgi:application", "-b", "0.0.0.0:8000", "--reload"]