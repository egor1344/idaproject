from PIL import Image
from idaproject.settings import BASE_DIR, MEDIA_URL


class ImageEditor:

    MEDIA_DIRECTORY = str(BASE_DIR) + MEDIA_URL

    def __init__(self, image_file: str) -> None:
        self.image = Image.open(self.MEDIA_DIRECTORY + str(image_file))

    def resize_image_by_width(self, new_width: int) -> Image:
        width_percent = (new_width / float(self.image.size[0]))
        new_height = int((float(self.image.size[1]) * float(width_percent)))
        image = self.image.resize((new_width, new_height), Image.NEAREST)
        return image

    def resize_image_by_height(self, new_height: int) -> Image:
        height_percent = (new_height / float(self.image.size[1]))
        new_width = int((float(self.image.size[0]) * float(height_percent)))
        image = self.image.resize((new_width, new_height), Image.NEAREST)
        return image

    def resize_image_by_width_and_height(self, new_width: int, new_height: int) -> Image:
        image = self.image.resize((new_width, new_height), Image.NEAREST)
        return image
