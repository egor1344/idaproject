import uuid
import requests
import random
import string
import os
from typing import Tuple
from django.db.models import QuerySet
from idaproject.settings import BASE_DIR, MEDIA_URL
from images.domain.models import Image, ImageFile
from images.domain.services.image_editor import ImageEditor


class ImageFileManager:

    @classmethod
    def get_image_file(cls, image_file_guid: uuid.uuid4) -> ImageFile:
        image_file = ImageFile.objects.get(guid=image_file_guid)
        return image_file


class ImageManager:

    MEDIA_DIRECTORY = str(BASE_DIR) + MEDIA_URL

    @classmethod
    def get_all_images(cls) -> QuerySet:
        images = Image.objects.all().order_by('-date')
        return images

    @classmethod
    def get_image(cls, image_guid: uuid.uuid4) -> Image:
        image = Image.objects.prefetch_related('image_files').get(guid=image_guid)
        return image

    @classmethod
    def upload_image(cls, initial_data: dict) -> None:
        if initial_data.get('file'):
            image_name = initial_data.get('file').name
            image_file = initial_data.get('file')
        else:
            image_name, image_file = cls._upload_image_from_url(initial_data.get('url'))
        image = Image.objects.create(guid=uuid.uuid4(), name=image_name)
        image_file = ImageFile.objects.create(guid=uuid.uuid4(), image=image, file=image_file)

    @classmethod
    def edit_image(cls, edit_params: dict, image: Image) -> None:
        image_file = image.image_files.all().order_by('date').first().file
        image_editor = ImageEditor(image_file=image_file)
        if edit_params.get('width') and edit_params.get('height'):
            resized_image_file = image_editor.resize_image_by_width_and_height(new_width=edit_params.get('width'),
                                                                               new_height=edit_params.get('height'))
        elif edit_params.get('width'):
            resized_image_file = image_editor.resize_image_by_width(new_width=edit_params.get('width'))
        else:
            resized_image_file = image_editor.resize_image_by_height(new_height=edit_params.get('height'))
        new_file_name = cls._add_some_text(image.name)
        resized_image_file.save(cls.MEDIA_DIRECTORY + new_file_name)
        image_file = ImageFile.objects.create(guid=uuid.uuid4(), image=image, file=new_file_name)

    @classmethod
    def _upload_image_from_url(cls, url: str) -> Tuple[str, str]:
        image_name = url.split('/')[-1]
        image_file = image_name
        response = requests.get(url=url)
        if response.status_code == 200:
            if cls._check_same_name_file(image_name):
                image_file = cls._add_some_text(image_name)
            with open(cls.MEDIA_DIRECTORY + image_file, 'wb') as file:
                file.write(response.content)

            return image_name, image_file
        else:
            raise Exception('invalid url')

    @classmethod
    def _add_some_text(cls, old_string: str) -> str:
        new_string = (part_list := old_string.split('.', 1))[0] + '_' + ''.join(
            random.choice(string.ascii_letters + string.digits) for _ in range(7)) + '.' + part_list[-1]
        return new_string

    @classmethod
    def _check_same_name_file(cls, name: str) -> bool:
        file_list = [file for file in os.listdir(cls.MEDIA_DIRECTORY)]
        return True if name in file_list else False
