# Generated by Django 3.2.4 on 2021-06-24 13:06

import datetime
from django.db import migrations, models
from django.utils.timezone import utc
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('images', '0003_auto_20210624_1250'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 24, 13, 6, 11, 818620, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='image',
            name='guid',
            field=models.UUIDField(default=uuid.UUID('43c05d25-56e0-43e3-ac8b-4ba4bb4e4a5c'), primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='imagefile',
            name='guid',
            field=models.UUIDField(default=uuid.UUID('219d7416-9ba4-4900-bc8b-d31b74901d45'), primary_key=True, serialize=False),
        ),
    ]
